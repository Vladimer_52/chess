﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chease
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }

   public class Movement
    {
        protected Point StartPosition { get; set; }

        protected Point CurrentPosition { get; set; }
        protected Point NextPosition { get; set; }
        protected bool WB { get; set; }

        public void Update()
        {
            //передвинуть Image с CurrentPosition на NextPosition

        }

    }

    public class ChessKing :Movement
    {
        public ChessKing(Point startPoint, bool whiteBlack)
        {
            StartPosition = startPoint;
            WB = whiteBlack;
        }
   
        public void Move(Point currentPoint ,Point nextPoint)
        {
            if(nextPoint.X < 0 && nextPoint.X > 8 &&
                nextPoint.Y < 0 && nextPoint.Y > 8)
            {
                MessageBox.Show("Ход не возможен!");
            }
            else
            {
                currentPoint = nextPoint;
                Update();

            }
        }

    }

}
